Installation
============

This profile requires several modules and must be installed with the full distribution. You can download the LevelTen Distribution Archive at http://www.leveltendesign.com/l10apps/cms/download.

For installation instructions visit http://www.leveltendesign.com/l10apps/cms/docs/install


Non Standard Modules
==============
- Freelinking 
  - change freelink path from freelinking/ to wiki/
- FileField
  - Bug in current release, use dev version
- RootCandy (theme)
  - Add phptemplate_tinymce_theme function (enables IMCE icon in TinyMCE image popup)
- TinyMCE
  - Additions to theme_tinymce_theme to remove filter from select textareas


Credits
=======
Created and maintained by Tom McCracken (tomdude48) and Dustin Currie.
Sponsored by LevelTen Interactive.