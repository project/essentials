core = "6.16"

projects[drupal][version] = "6.16"

;;;;;;
; sitebuilding tools
;;;;;;

; Allows administrators to define new content types.
projects[cck][subdir] = "contrib"
projects[cck][version] = "2.6"

; Defines a file field type.
projects[filefield][subdir] = "contrib"
projects[filefield][version] = "3.2"

; Defines an image field type.
projects[imagefield][subdir] = "contrib"
projects[imagefield][version] = "3.2"

; Dynamic image manipulator and cache.
projects[imagecache][subdir] = "contrib"
projects[imagecache][version] = "2.0-beta1"

; ImageAPI supporting multiple toolkits.
projects[imageapi][subdir] = "contrib"
projects[imageapi][version] = "1.6"

; Create customized lists and queries from your database.
projects[views][subdir] = "contrib"
projects[views][version] = "2.10"

; A library of helpful tools by Merlin of Chaos.
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

; Defines simple link field types.
projects[link][subdir] = "contrib"
projects[link][version] = "2.8"


; Core Panels display functions; provides no external UI, at least one other Panels module should be enabled.
projects[panels][subdir] = "contrib"
projects[panels][version] = "3.3"

; Create queues which can contain nodes in arbitrary order
projects[nodequeue][subdir] = "contrib"
projects[nodequeue][version] = "2.9"

; Provides a shared API for replacement of textual placeholders with actual data.
projects[token][subdir] = "contrib"
projects[token][version] = "1.12"

; Provide modules with a cache that lasts for a single page request.
projects[context][subdir] = "contrib"
projects[context][version] = "2.0-beta7"

; Exposes new Views style 'Bulk Operations' for selecting multiple nodes and applying operations on them.
projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "1.9"

; Provides a customizable spaces layer on top of Drupal.
projects[spaces][subdir] = "contrib"
projects[spaces][version] = "2.0-beta7"

;;;;;;;
; content tools
;;;;;;

; Enables the usage of CKEditor (WYSIWYG) instead of plain text fields.
projects[ckeditor][subdir] = "contrib"
projects[ckeditor][version] = "1.1"

; Adds a printer-friendly version link to content and administrative pages.
projects[print][subdir] = "contrib"
projects[print][version] = "1.10"

; Wiki-style freelinking for node content using CamelCase and delimiters.
projects[freelinking][subdir] = "contrib"
projects[freelinking][version] = "1.10"

; An image/file uploader and browser supporting personal directories and user quota.
projects[imce][subdir] = "contrib"
projects[imce][version] = "1.3"

; Enables Lightbox2 for Drupal
projects[lightbox2][subdir] = "contrib"
projects[lightbox2][version] = "1.9"

; Tagadelic makes weighted tag clouds from your taxonomy terms.
projects[tagadelic][subdir] = "contrib"
projects[tagadelic][version] = "1.2"

; Adds a 'Tagadelic' style option to views
; projects[views_tagadelic][subdir] = "contrib"

; Provides helper functionality to have wiki-like behaviour.
projects[wikitools][subdir] = "contrib"
projects[wikitools][version] = "1.2"

; Enables the creation of forms and questionnaires.
projects[webform][subdir] = "contrib"
projects[webform][version] = "2.9"

; This is the core component of SWF Tools and provides a common API for handling Adobe Flash related media.
projects[swftools][subdir] = "contrib"
projects[swftools][version] = "2.5"

;;;;;;
; community building tools
;;;;;; 

; Use content types for user profiles.
projects[content_profile][subdir] = "contrib"
projects[content_profile][version] = "1.0"

; Create customized flags that users can set on content.
projects[flag][subdir] = "contrib"
projects[flag][version] = "1.2"

; Creates AddThis button as a block, to be used in themes and to node links.
projects[addthis][subdir] = "contrib"
projects[addthis][version] = "2.9"

; A simple five-star voting widget for nodes.
projects[fivestar][subdir] = "contrib"
projects[fivestar][version] = "1.19"

; Provides a shared voting API for other modules.
projects[votingapi][subdir] = "contrib"
projects[votingapi][version] = "2.3"

; Messaging system. This is the base module for the Messaging Framework
projects[messaging][subdir] = "contrib"
projects[messaging][version] = "2.2"

; The basic notifications framework
projects[notifications][subdir] = "contrib"
projects[notifications][version] = "2.2"

; Allow private messages between users.
projects[privatemsg][subdir] = "contrib"
projects[privatemsg][version] = "1.1"

; Display comments on a seperate talk page
projects[talk][subdir] = "contrib"
projects[talk][version] = "1.6"

; Make all comments replies to the main post, regardless of the reply link used.
projects[flatcomments][subdir] = "contrib"
projects[flatcomments][version] = "2.0"

; Allows users to 'tag' other users' content. Weighted tag lists or 'tag clouds' are generated for each piece of content to show the popularity of tags.
projects[community_tags][subdir] = "contrib"
projects[community_tags][version] = "1.0-beta3"

; Provides a filter to highlight source code using the GeSHi library (Generic Syntax Highlighter)
projects[geshifilter][subdir] = "contrib"
projects[geshifilter][version] = "1.3"

; Uses the Disqus web service to enhance comments.
projects[disqus][subdir] = "contrib"
projects[disqus][version] = "1.6"

;;;;;;
; aggregation
;;;;;;

; Provides feed aggregation functionality, a feed management interface and an API.
projects[feedapi][subdir] = "contrib"
projects[feedapi][version] = "1.9-beta3"

; Maps feed item elements to node fields.
projects[feedapi_mapper][subdir] = "contrib"
projects[feedapi_mapper][version] = "1.3"

;;;;;;
; search
;;;;;;

; Improve English language searching by simplifying related words to their root (conjugations, plurals, ...)
projects[porterstemmer][subdir] = "contrib"
projects[porterstemmer][version] = "2.5"

; For page not founds, searches the values entered after the domain name
projects[search404][subdir] = "contrib"
projects[search404][version] = "1.9"

;;;;;;
; exportables
;;;;;;

; Provides feature management for Drupal content types, views, imagecache.
projects[features][subdir] = "contrib"
projects[features][version] = "1.0-beta6"

; Provides exports for custom blocks and spaces integration.
projects[boxes][subdir] = "contrib"
projects[boxes][version] = "1.0-beta4"

; Allows users to export a node and the import into another Drupal installation.
projects[node_export][subdir] = "contrib"
projects[node_export][version] = "2.21"

; Enforces variable values defined by modules that need settings set to operate properly.
projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0-rc1"

;;;;;;;
; time date location
;;;;;;

; Views plugin to display views containing dates as Calendars.
projects[calendar][subdir] = "contrib"
projects[calendar][version] = "2.2"

; Date API that can be used by other modules.
projects[date][subdir] = "contrib"
projects[date][version] = "2.4"

; Filter to allow insertion of a google map into a node
projects[gmap][subdir] = "contrib"
projects[gmap][version] = "1.1-rc1"

; The location module allows you to associate a geographic location with content and users.
projects[location][subdir] = "contrib"
projects[location][version] = "3.1-rc1"

;;;;;;
; admin tools
;;;;;;

; UI helpers for Drupal admins and managers. Includes a special admin theme, custom admin header with JS support, and contextual admin links.
projects[admin][subdir] = "contrib"
projects[admin][version] = "1.0-beta3"

; Backup or migrate the Drupal Database quickly and without unnecessary data.
projects[backup_migrate][subdir] = "contrib"
projects[backup_migrate][version] = "2.2"

; Provides a dropdown menu to most administrative tasks and other common destinations (to users with the proper permissions).
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "1.5"

; Allow advanced help and documentation.
projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.2"

;;;;;;
; spam
;;;;;;

; Unobtrusive form spam control
; projects[uncaptchalous][subdir] = "contrib"

; Base CAPTCHA module for adding challenges to arbitrary forms.
projects[captcha][subdir] = "contrib"
projects[captcha][version] = "2.1"

; Uses the reCAPTCHA web service to improve the CAPTCHA system.
projects[recaptcha][subdir] = "contrib"
projects[recaptcha][version] = "1.4"

; Obfuscates email addresses to help prevent spambots from collecting them.
projects[spamspan][subdir] = "contrib"
projects[spamspan][version] = "1.5-beta2"

;;;;;;
; profile
;;;;;;

; Utility functions that help with install profile creation and running
projects[install_profile_api][subdir] = "contrib"
projects[install_profile_api][version] = "2.1"

;;;;;;
; theme
;;;;;;

; Allows themes to add conditional stylesheets.
projects[conditional_styles][subdir] = "contrib"
projects[conditional_styles][version] = "1.1"

;;;;;;
; dev tools
;;;;;;

; Create snapshots and reset the site for demonstration or testing purposes.
projects[demo][subdir] = "contrib"
projects[demo][version] = "1.4"

; Various blocks, pages, and functions for developers.
projects[devel][subdir] = "contrib"
projects[devel][version] = "1.19"

; Show difference between node revisions.
projects[diff][subdir] = "contrib"
projects[diff][version] = "2.0"

; An API and home for miscellaneous jQuery plugins.
projects[jquery_plugin][subdir] = "contrib"
projects[jquery_plugin][version] = "1.10"

; The Schema module provides functionality built on the Schema API.
projects[schema][subdir] = "contrib"
projects[schema][version] = "1.7"

;;;;;;
; passive utilities
;;;;;;

; Input filter to convert internal paths, such as internal:node/99&quot;, to their corresponding absolute URL or relative path.
projects[pathfilter][subdir] = "contrib"
projects[pathfilter][version] = "1.0"

; Provides a mechanism for modules to automatically generate aliases for the content they manage.
projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"

; Searches for an alias of the current URL and 301 redirects if found. Stops duplicate content arising when path module is enabled.
projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = "1.2"

; Provides tools for using persistant URL pieces
projects[purl][subdir] = "contrib"
projects[purl][version] = "1.0-beta1"

;;;;;;
; seo tools
;;;;;;

; Adds Google Analytics javascript tracking code to all your site's pages.
projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "2.2"

; Allows users to add meta tags, e.g. keywords or description. This module doesn't actually implement any meta tags, but requires other modules to implement them.
projects[nodewords][subdir] = "contrib"
projects[nodewords][version] = "1.11"

; Analyzes node content for search engine optimization recommendations
projects[contentoptimizer][subdir] = "contrib"
projects[contentoptimizer][version] = "2.0-beta1"

; Enhanced control over the page title (in the &lt;head> tag).
projects[page_title][subdir] = "contrib"
projects[page_title][version] = "2.3"

; Displays top searches
; projects[sitesearchstats][subdir] = "contrib"

; Analyze Content
projects[contentanalysis][subdir] = "contrib"
projects[contentanalysis][version] = "1.0-beta1"

; Readability Analyzer
projects[readability][subdir] = "contrib"
projects[readability][version] = "1.0-beta1"

; ScribeSEO Analyzer
projects[scribeseo][subdir] = "contrib"
projects[scribeseo][version] = "1.0-beta1"

; WC3 Analyzer
projects[w3canalyzer][subdir] = "contrib"
projects[w3canalyzer][version] = "1.0-beta1"

; Content Optimizer Analyzer
projects[contentoptimizer][subdir] = "contrib"
projects[contentoptimizer][version] = "2.0-beta1"

;;;;;;;
; Essential Features
;;;;;;;

projects[l10_bios][subdir] = "features"
projects[l10_bios][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_bios][version] = "1.5"

projects[l10_blogs][subdir] = "features"
projects[l10_blogs][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_blogs][version] = "1.2"

projects[l10_boxes][subdir] = "features"
projects[l10_boxes][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_boxes][version] = "1.0"

projects[l10_events][subdir] = "features"
projects[l10_events][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_events][version] = "1.2"

projects[l10_faqs][subdir] = "features"
projects[l10_faqs][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_faqs][version] = "1.1"

projects[l10_input_filters][subdir] = "features"
projects[l10_input_filters][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_input_filters][version] = "1.0"

projects[l10_news_article][subdir] = "features"
projects[l10_news_article][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_news_article][version] = "1.0"

projects[l10_news_room][subdir] = "features"
projects[l10_news_room][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_news_room][version] = "1.0"

projects[l10_pages][subdir] = "features"
projects[l10_pages][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_pages][version] = "1.0"

projects[l10_pathauto][subdir] = "features"
projects[l10_pathauto][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_pathauto][version] = "1.0"

projects[l10_press_releases][subdir] = "features"
projects[l10_press_releases][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_press_releases][version] = "1.0"

projects[l10_profile][subdir] = "features"
projects[l10_profile][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_profile][version] = "1.0"

projects[l10_theme][subdir] = "features"
projects[l10_theme][location] = "http://essentials.leveltendesign.com/fserver"
projects[l10_theme][version] = "1.1"


;;;;;;
; themes
;;;;;;

projects[officebobbles][version] = "1.0-beta1"


;;;;;;
; profiles
;;;;;;
projects[l10_essentials][location] = "http://essentials.leveltendesign.com/fserver"

